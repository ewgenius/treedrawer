package com.ewgenius.treedrawer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.EllipseBuilder;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 05.08.13
 * Time: 8:55
 * To change this template use File | Settings | File Templates.
 */
public class MainApp extends Application {
    Scene scene;
    AnchorPane canvas;

    @Override
    public void start(Stage primaryStage) {
        try {
            primaryStage.setTitle("Hello World!");
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/MainWindow.fxml"));
            AnchorPane root = (AnchorPane) loader.load();
            scene = new Scene(root);

            primaryStage.setScene(scene);
            primaryStage.show();

            canvas = (AnchorPane) ((ScrollPane) root.getChildren().get(0)).getContent();
            // Object o = scene.lookup("Content");

            Ellipse circle = EllipseBuilder.create()
                    .centerX(100)
                    .centerY(100)
                    .radiusX(30)
                    .radiusY(30)
                    .strokeWidth(3)
                    .stroke(Color.BLACK)
                    .fill(Color.BLUE)
                    .build();


            canvas.autosize();

            canvas.getChildren().add(circle);

        } catch (IOException e) {

        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

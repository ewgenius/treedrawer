package com.ewgenius.treedrawer.structures;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ewgenius
 * Date: 05.08.13
 * Time: 9:26
 * To change this template use File | Settings | File Templates.
 */
public class Tree<Type> {
    public class TreeNode {
        private int id;
        private Type value;
        private TreeNode parent;
        private ArrayList<TreeNode> children = new ArrayList<TreeNode>();

        public TreeNode(TreeNode parent) {
            this.parent = parent;
        }

        public void setValue(Type value) {
            this.value = value;
        }

        public Type getValue() {
            return value;
        }

        public int getId() {
            return id;
        }
    }

    private TreeNode root;

    public Tree() {

    }
}
